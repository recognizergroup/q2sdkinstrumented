from setuptools import find_packages, setup

from q2_sdk.version import __version__

setup(
    name='q2-sdk-instrumented',
    version=__version__,
    description='framework to instrument SDK replace logging with trace recordings',
    author='Peter Kong',
    author_email='peter.kong@q2.com',
    packages=find_packages(),
    package_data={
        'q2_sdk_instrumented': []
    },
    python_requires='>=3',
    install_requires=[
        'q2-sdk~=2.216.0'
    ],
    entry_points={},
    scripts=[],
)
